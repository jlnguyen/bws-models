# ==============================================================
'''
SCRIPT:
	crt_sql_files.py
	
PURPOSE:
    - Create multiple sql files using template <response_model.sql>
    - Update {ref_date_to_replace} in sql template file with list of dates in <ref_dt.txt>

    Please exclude data in the below periods for building the model
        From 2019-12-12 To 2020-01-09 (Christmas, New Year)
        From 2020-01-23 To 2020-01-30 (Data issue)
        From 2020-03-02 To now (COVID 19)
INPUTS:
	
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 15Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os
import sys
import pandas as pd
import fileinput
import glob

# file_ref_dt = 'ref_dt.txt'
# file_ref_dt_excl = 'ref_dt_excl.txt'
file_ref_dt = 'ref_dt_rohit.txt'

# Exclusion dates
excl = pd.DataFrame(
    [['2019-12-12', '2020-01-09', 'xmas'],
     ['2020-01-23', '2020-01-30', 'data issue'],
     ['2020-03-02', '2020-04-15', 'COVID-19']],
    columns=['from', 'to', 'reason']
)


def write_all_sql_file(file_sql, file_sql_all, file_ref_dt, is_excl=False):
    '''Create a sql file with all dates from <ref_dt.txt> where the date is
    used to replace placeholders "ref_date_to_replace" and "refdatetoreplace"
    in the original sql file

    Args:
        file_sql (str): Template sql file
        file_sql_all (str): output sql file with all dates

    Returns:
        None

    Raises:
    '''
    # -------------------------------------------------
    # Load
    # -------------------------------------------------
    ref_dt_ls = pd.read_table(file_ref_dt, header=None)[0].tolist()

    # Exclude ref_dt
    if is_excl:
        excl_dt = []
        for dt in ref_dt_ls:
            for i in excl.index:
                if excl.loc[i, 'from'] <= dt <= excl.loc[i, 'to']:
                    excl_dt.append(dt)
        ref_dt_ls = sorted(set(ref_dt_ls) - set(excl_dt))

        with open(file_ref_dt, 'w') as f:
            f.write("\n".join(ref_dt_ls))

    # -------------------------------------------------
    # Process
    # -------------------------------------------------
    for dt in ref_dt_ls:
        with open(file_sql, 'r') as f:
            filedata = f.read()

        filedata = filedata.replace('ref_date_to_replace', dt)
        filedata = filedata.replace('refdatetoreplace', dt.replace('-', ''))

        file_part = file_sql.partition('.')
        file_sql_new = file_part[0] + '_' + dt + file_part[1] + file_part[2]

        with open(file_sql_new, 'w') as file:
            file.write(filedata)

    # ==============================================================
    # Combine scripts into one
    # ==============================================================
    file_part = file_sql.partition('.')[0] + '_*'
    file_list = sorted(glob.glob(file_part))

    with open(file_sql_all, 'wb') as outfile:
        for f in file_list:
            with open(f, "rb") as infile:
                outfile.write(infile.read())

    return None


def main():
    # write_all_sql_file('response_model.sql',
    #    'all_response_model.sql', file_ref_dt)
    write_all_sql_file('previous_model.sql',
                       'all_previous_model.sql', file_ref_dt)


main()
