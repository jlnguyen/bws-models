-------------------------------------------------
-- 2020-03-05
-------------------------------------------------
drop table if exists loyalty_modeling.de_resp_mdl_20200415__20200305;
create table loyalty_modeling.de_resp_mdl_20200415__20200305
distkey (crn) as

--==============================================================
/*
    Original code
*/
--==============================================================
with ref_dt as(
	-- select next_day(CONVERT_TIMEZONE('AEDT',getdate())::DATE - 28,'Thursday')  as dt
    select '2020-03-05'::date as dt
),
--- base table
base as (
	SELECT distinct (select dt from ref_dt) as ref_dt, crn, offer_id, selection, action, offer_start_dt, offer_end_dt, var_2, var_1 as sample_weight
	FROM loyalty_nba.processed_decision
	WHERE  (reminder_f = 'NULL' or  reminder_f = '' ) and 
	case when offer_start_dt <> '' 
	then cast(offer_start_dt as date) end = (select dt from ref_dt)
),
--- sales table
sales_tbl as (
	select	
		crn,
	    SUM(CASE WHEN ASS.start_txn_date BETWEEN (select dt from ref_dt) AND (select dt + 6 from ref_dt) 
			THEN ASS.tot_amt_incld_gst - ASS.tot_wow_dollar_incld_gst ELSE 0 END) as sale_7day,
		SUM(CASE WHEN ASS.start_txn_date BETWEEN (select dt from ref_dt) AND (select dt + 13 from ref_dt) 
			THEN ASS.tot_amt_incld_gst - ASS.tot_wow_dollar_incld_gst ELSE 0 END) as sale_14day		
	from               loyalty.article_sales_summary ass
	INNER JOIN loyalty.lylty_card_detail LCD ON LCD.lylty_card_nbr = ASS.lylty_card_nbr
	INNER JOIN loyalty.article_master am
	on         ass.prod_nbr = am.prod_nbr
	and        ass.division_nbr = am.division_nbr
	where      ass.division_nbr in (1005,1010,1030)
	and        am.dept_code = 20 -- Liquor
	and        mrcds_ctgry_lvl_2_name = 'BEVERAGES ALCOHOLIC'
	and        ass.void_flag<>'Y'
	and        ass.start_txn_Date > (select dt - 1 from ref_dt)
	and        (ass.tot_amt_incld_gst-coalesce(ass.tot_wow_dollar_incld_gst,0)) > 0
	group by crn
),
--- virtual offer redemptions
redeem as (
  select crn, offer_nbr, offer_alloc_start_ts, 1 as redemption
  from loyalty.campaign_exec_reward 
  where offer_alloc_start_ts = (select dt from ref_dt)
  and offer_nbr in (select distinct offer_id from base)
  and reward_credit_status <> 'Credit Failed'
  group by 1, 2, 3
),
--- lms offer redemptions
lms_offers as (
	SELECT offer_nbr, alternate_offer_nbr
	FROM loyalty.offer
	WHERE alternate_offer_nbr <> '' and alternate_offer_nbr in (select distinct offer_id from base)
),
lms_offers_redeem as (
	SELECT LCD.crn, offer_nbr, LEAST(COUNT(DISTINCT basket_key), 1) as lms_redeem_times
	FROM loyalty.sales_reward_detail as SRD
	INNER JOIN loyalty.lylty_card_detail LCD 
	ON SRD.lylty_card_nbr = LCD.lylty_card_nbr
	WHERE SRD.start_txn_date > (SELECT dt - 1 from ref_dt) and SRD.offer_nbr in (select distinct offer_nbr from lms_offers)
	group by 1, 2
),
lms_offer_output as (
	select a.crn, b.alternate_offer_nbr as offer_id, a.lms_redeem_times
	from lms_offers_redeem as a
	left join lms_offers as b
	on a.offer_nbr = b.offer_nbr
),
--- engagement
engagement as (
  SELECT
    -- date_trunc('day', dateadd('hour',6,CONVERT_TIMEZONE('Australia/Sydney',sl.sfmc_datetime)))::date campaign_start_date, 
    date_trunc('day', dateadd('hour',6,CONVERT_TIMEZONE('Australia/Sydney',sl.sfmc_datetime)))::datetime sfmc_datetime,
    sl.subscriber_key as crn,
    COUNT(distinct CASE WHEN o.subscriber_id IS NOT NULL THEN sl.subscriber_key ELSE null END) as open_flag,
    COUNT(distinct CASE WHEN un.subscriber_id IS NOT NULL THEN sl.subscriber_key ELSE null END) as unsub_flag
  FROM loyalty.et_resp_sendlog sl

  /* Get open event related to email */
  LEFT JOIN loyalty.et_resp_open o
  on sl.send_id = o.send_id
  and sl.subscriber_id = o.subscriber_id

  /* Get unsub event related to email */
  LEFT JOIN loyalty.et_resp_unsubs un
  on sl.send_id = un.send_id
  and sl.subscriber_id = un.subscriber_id

  where     
      date_trunc('day',dateadd('hour',6,CONVERT_TIMEZONE('Australia/Sydney',sl.sfmc_datetime)))
	  BETWEEN  (SELECT dt - 1 from ref_dt) and (SELECT dt + 13 from ref_dt)
      and campaign_code = 'NBA-0001'
  GROUP BY 1,2
),
engagement_output as(
	select a.crn, a.offer_start_dt, a.offer_end_dt,
		LEAST(sum(open_flag), 1) as open_flag,
		LEAST(sum(unsub_flag), 1) as unsub_flag
	from base a
	inner join engagement b
	on a.crn = b.crn and b.sfmc_datetime between a.offer_start_dt and a.offer_end_dt
	group by 1, 2, 3
)
---
	select a.*, NVL(e.open_flag, 0) as open_flag, NVL(e.unsub_flag, 0) as unsub_flag,
	NVL(b.redemption, d.lms_redeem_times, 0) as redeem_flag,
	NVL(c.sale_7day, 0) as sale_7day, NVL(sale_14day, 0) as sale_14day
	from base as a
	left join redeem as b
	on a.crn = b.crn and a.offer_start_dt = b.offer_alloc_start_ts
	left join sales_tbl as c
	on a.crn = c.crn
	left join lms_offer_output as d
	on a.crn = d.crn and a.offer_id = d.offer_id
	left join engagement_output as e
	on a.crn = e.crn and a.offer_start_dt = e.offer_start_dt;


--==============================================================
/*
    Copy to S3
*/
--==============================================================
UNLOAD ('select * from loyalty_modeling.de_resp_mdl_20200415__20200305')
TO 's3://data-preprod-redshift-exports/Joe/decision-engine/response/2020-03-05'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
FORMAT PARQUET
ALLOWOVERWRITE
PARALLEL OFF;

drop table if exists loyalty_modeling.de_resp_mdl_20200415__20200305;
