##################################################################################
#                                                                                #
#                                    AKL V0.5                                    #
#                                                                                #
##################################################################################
# Decision Engine | RO with prev | reg: sales | 1.5M rows
global:
  modeller: Joe Nguyen
  google_project: wx-bq-poc
  google_bucket: wx-decision-engine
  google_subdir: models
  model_name: target_spd
  run_date: '2020-04-16-cmd2-with-prev'
  objective: regression
  target: sales
  sample_weight: sample_weight      # User defined weight column, otherwise leave as sample_weight
  ids:                              # Variables to keep in output, but not used for training
  - crn
  - ref_dt
  metric:
      # Metrics that are used during training and available metrics include:
      # For regression: l1, l2, mape, map, rmse, quantile, mse, kullback_leibler
      # For Classification: auc, cross_entropy,  binary_logloss,  binary_error, kullback_leibler
  - rmse
  - l1
input:
  data_addr: wx-decision-engine/data/model_data/w_cmd/2020-04-16/*
    # For single file, use the exact file path:
    # e.g.: wx-auto-ml/test_branch/sample/rdm_data_reduce.parquet
    # For partitions, use the folder path followed by *:
    # e.g.: wx-auto-ml/test_branch/sample/*
  config_addr: wx-personal/Sean/Raw_DE_data/config/RO_cmd2_prev.xlsx
    # For excel files on gcs:
    # e.g.: wx-auto-ml/test_branch/sample/RdmLabel.xlsx
    # For google sheet formats on google drive, it should be the url of the file:
    # e.g.: https://docs.google.com/spreadsheets/d/1ttLSimKGRKu_nIXUqBVVkis9NFzfhHfX823AuVU2Q04/edit#gid=87460757
# ------------------------------------------------------------------------------
preprocessor:
  n_cpu : 30
      # Number of CPU that is used for preproceessing and generating config
      # It is an optional feature and the default number is maximun cpu - 5
      # If your data is larger than 5 million, you should use a smaller number cpu
  chunk_size : 30
      # Number of features in chunk and default number is 30
      # If your data is larger than 5 million, you should use a smaller number features
  sparsity: 0.999
      # Sparsity threshold for filter out sparse data, ranging from 0 to 1 
      # It is an optional feature and default number is 1 which means turning off the filter
      # In that is 1, it only filters the features with only none or features with one unique value
  target_missing_handle: keep
      # Two options: keep/remove
      # keep: records with missing target are filled with 0
      # remove: remove the records with missing target
  params:
    sampling:
      sample_on: train # train or full
      # Option 1: uniform sampling by target value
      # value: 0, 1
      # rate: 1, 1
      # Option 2: uniform sampling for the whole data
      rate: 1
      random_state: 123 # Random seed of generating test set
    train_test_holdout:
      # Two options: by_time/by_proportion
      # by_proportion: only generate train/test set
      # by_time: generate holdout set with user define time threshold
      #          The final output will have train/test/holdout set
      split_type: by_time
      by_proportion: 80 # Train size, 0-100 or 0.0 - 1.0
      by_time:
        format: '%Y-%m-%d' # Time format for holdout
        timeVar: ref_dt # Define the variable for time variable
        inTimeEndDate: '2019-11-10' # Define the end time of training data
        inTimeTrainTestSplit: 70 # Training size, same with 'by_proportion'
# ------------------------------------------------------------------------------
feature_selection:
  skip: false
  # After V0.5, we add the skip key to skip feature selection.
  iteration: 0, 2, 4
  # After V0.5, we support multi-level feature selection.
  # If you set it to '0,2,4',
  # the first and second iterations are using the first set of parameter
  # the 3nd and 4th iterations are using the second set of parameter
  # after the 4th iteration, all of iterations are using the third set of parameter
  hard_sampling: 0.2, 0.4, 0.6
  # After V0.5, we support multi-level hard_sampling in feature selection.
  # You should set the same number of and order with iteration.
  # In this case
  # the first two iterations will use 20% of training data for feature selection
  # the 3nd and 4th iterations will use 40% of training data for feature selection
  # after the 4th iteration, all of iterations will use 60% of training data for feature selection
  training_params:
    label_encode: true # Enable label encode or not: true/ false
    early_stopping_rounds: 50
    random_state: 777
    model_spec:
      # After V0.5, we support multi-level hard_sampling in feature selection.
      # You should set the same number of and order with iteration
      # Or just 1 parameter which means that all iteration will use the same set of parameter

      # AKL supports more than these parameters
      # Detailed information could be found at
      # https://lightgbm.readthedocs.io/en/latest/Parameters.html
      n_estimators: 1500,2000,2500 #2000
      max_depth: 5,6,8 #8
      num_leaves: 30,50,80 #50
      min_data_in_leaf: 100
      learning_rate: 0.1,0.05,0.03 #0.03
      random_state: 123
      reg_alpha: 0.1
      reg_lambda: 0.1
      sub_feature: 0.8
      bagging_freq: 1
      bagging_fraction: 0.9
  CV: 3               # Number of cross validation folds, cannot be set to 1 # 4
  # feature importance is relative to the feature with maximum importance
  min_imp: .0005     # minimum importance required for selection # 0.00001, .0005, .0001
  inc_imp: .0005      # size of increment in cut-off threshold    # 0.00001, .001, .0005
  min_var: 40        # stopping condition 1: stop after cutting below this threshold
  max_step: 20        # stopping condition 2: stop after reaching number of iterations
  min_cut: 0.05
  max_cut: 0.9
  # After V0.5, we introduce min_cut and max_cut paremeters which are option.
  # These parameters are controling the presentage of features being dropped at each iteration.
  # min_cut's default value is 0 and max_cut's default value is 1
  # min_cut should be smaller than max_cut, and both are ranging from 0 to 1
  # min_cut represents the min presentage of feature being dropped at each iteration.
  # max_cut represents the max presentage of feature being dropped at each iteration.
  output_iteration: best
  # After V0.5, we support an new function that user can select the iteration they want
  # If you put best, we will select the best iteration, according to the validation score
  # If you put integer like 5, we will select the features from the 5th iteration
  # If you put -1, we will just output the features selected from the last iteration
  forceInputFeature:
  - f0_multiplier
  - f0_campaign_level
  - f0_cvm
  - f0_campaign_duration
  - f0_campaign_category
  - f0_reward
  # - ''
      # White list of features that be forced into fitting
      # Example:
      # - 'f0_campaign_type'
# ------------------------------------------------------------------------------
fit:
  training_params:
    label_encode: true
    early_stopping_rounds: 50
    nfolds: 5                 # Number of cross validation folds, can be set to 1
    train_rate: 70            # If nfolds=1, 70% of data will be used for training
    random_state: 777
  algo_max_tune:
    # Number of random grid search iterations for each boosting type
    gbdt: 19
    rf: 1
    # dart: 4 (optional)
    # AKL supports more than these parameters
    # Detailed information could be found at
    # https://lightgbm.readthedocs.io/en/latest/Parameters.html
  gbdt_hyper_params:
    n_estimators:  1000, 1500, 2000, 2500
    num_leaves: 30,20,25
    learning_rate: 0.08, 0.1, 0.05
    max_depth: 3,4,5
    min_child_samples: 100,200,500
    reg_alpha: 0.01, 0.02, 0.03, 0.1, 0.5, 1
    reg_lambda: 0.01, 0.02, 0.04, 0.1, 0.5 , 1
    random_state: 777
  rf_hyper_params:
    bagging_freq: 1,5,10
    bagging_fraction: 0.8, 0.7
    n_estimators:  1000, 1500, 2000
    num_leaves: 31, 50, 60, 70
    learning_rate: 0.01, 0.015 ,0.02
    max_depth: 4, 6, 8
    min_child_samples: 100,200,500
    reg_alpha: 0.01, 0.02, 0.03, 1
    reg_lambda: 0.01, 0.02, 0.04, 1
    random_state: 777
model_selector:
  user_specified_model: ''
      # Default to '', AKL selects iteration with best validation performance
      # User can overwrite with iteration number
      # Use diagnostic dashboard to see performance between iterations
      # Option 1: select the best validation performance
      # user_specified_model: ''
      # Option 2: select user define model like the first one
      # user_specified_model: 1
diagnosis:
  diag_groups: 20
      # Optional parameter and the default one is 10
      # Set number of groups for each feature to calculate the mean of prediction and actual
      # If your category feature unique value is larger than this, 
      # We will set to the number of max category feature unique value
  optional_features:
  - f0_multiplier
  - f0_campaign_level
  - f0_cvm
  - f0_campaign_duration
  - f0_campaign_category
      # Additional features for goodness of fit plots
  shap_para:
    cutoff_method: quantile # quantile or absolute
    min: 0.03
    max: 0.97
  metrics:
  - rmse
  - l1
  # After V0.4c, we support an new function that user can select metrics to be calculated in diag
  # For regression we have:
  #  explained_variance_score,mean_absolute_error,mean_squared_error,
  #  mean_absolute_error,mean_squared_log_error,median_absolute_error
  # For classification we have:
  #  accuracy_score,auc,average_precision_score,
  #  f1_score,log_loss,binary_logloss,
  #  precision_score,r2_score, r2,recall_score,zero_one_loss,binary_error