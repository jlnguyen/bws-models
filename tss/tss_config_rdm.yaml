##################################################################################
#                                                                                #
#                                    AKL V0.5                                    #
#                                                                                #
##################################################################################
# Decision Engine | Tier Spend Stretch | bin-cls: redeem_flag
global:
  modeller: Joe Nguyen          # First name and last name
  google_project: wx-bq-poc     # Do not change
  google_bucket: wx-auto-ml     # Do not change
  google_subdir: joe            # Can be any string
  model_name: tss_rdm            # Can be any string
  run_date: '2020-04-11'        # Can be any string
  objective: binary             # binary or regression task
  target: redeem_flag           # Name of target variable
  sample_weight: sample_weight  # User defined weight column, otherwise leave as sample_weight
  ids:                          # Variables to keep in output, but not used for training
  - crn
  - ref_dt
  metric:
  - auc
  - binary_logloss
input:
  data_addr: wx-personal/joe/a02_decision-engine/tss/data/w_cmd/*
    # For single file, use the exact file path:
    # e.g.: wx-auto-ml/test_branch/sample/rdm_data_reduce.parquet
    # For partitions, use the folder path followed by *:
    # e.g.: wx-auto-ml/test_branch/sample/*
  config_addr: wx-personal/joe/a02_decision-engine/tss/config/de_tss_config_rdm.xlsx # The path of excel config
    # For excel files on gcs:
    # e.g.: wx-auto-ml/test_branch/sample/RdmLabel.xlsx
    # For google sheet formats on google drive, it should be the url of the file:
    # e.g.: https://docs.google.com/spreadsheets/d/1ttLSimKGRKu_nIXUqBVVkis9NFzfhHfX823AuVU2Q04/edit#gid=87460757
# ------------------------------------------------------------------------------
preprocessor:
  n_cpu : 30
      # Number of CPU for parallel preprocessing
      # It is optional and the default number is maximum CPU - 5
  chunk_size : 30
      # Number of columns in each chunk of data
      # It is optional and the default number is 30
      # If it is too large, it will get into a memory issue.
      # If that is too small, it will slow down the process.
      # 30 - 50 is suggested range.
      # If data is too large like 60 mil, it should be smaller like 10
  sparsity: 0.999
      # The parameter is to control sparsity cut-off.
      # It ranges from 0 to 1.
      # If a feature's sparsity is larger than that, it will be filtered out.
      # The default value is 1 which only filter features with only none or one unique value.
  target_missing_handle: keep
      # Two options: keep/remove
      # keep: records with missing target are filled with 0
      # remove: remove the records with missing target
  params:
    sampling:
      sample_on: train # train or full
      # Option 1: uniform sampling by target value
      value: 0, 1
      rate: 0.1, 1
      # Option 2: uniform sampling for the whole data
      # rate: 0.3
      random_state: 123 # Random seed of generating test set
    train_test_holdout:
      # Two options: by_time/by_proportion
      # by_proportion: only generate train/test set
      # by_time: generate holdout set with user define time threshold
      #          The final output will have train/test/holdout set
      split_type: by_proportion
      by_proportion: 80 # Train size, 0-100 or 0.0 - 1.0
      by_time:
        format: '%Y-%m-%d' # Time format for holdout
        timeVar: ref_dt # Define the variable for time variable
        inTimeEndDate: '2018-12-01' # Define the end time of training data
        inTimeTrainTestSplit: 70 # Training size, same with 'by_proportion'
# ------------------------------------------------------------------------------
feature_selection:
  skip: false  
  # After V0.5, we add the skip key to skip feature selection.
  iteration: 0, 2, 4
  # After V0.5, we support multi-level feature selection.
  # If you set it to '0,2,4',
  # the first and second iterations are using the first set of parameter
  # the 3nd and 4th iterations are using the second set of parameter
  # after the 4th iteration, all of iterations are using the third set of parameter
  hard_sampling: 0.2, 0.4, 0.6
  # After V0.5, we support multi-level hard_sampling in feature selection.
  # You should set the same number of and order with iteration.
  # In this case
  # the first two iterations will use 20% of training data for feature selection
  # the 3nd and 4th iterations will use 40% of training data for feature selection
  # after the 4th iteration, all of iterations will use 60% of training data for feature selection
  training_params:
    label_encode: true # Enable label encode or not: true/ false
    early_stopping_rounds: 50
    random_state: 777
    model_spec:
      # After V0.5, we support multi-level hard_sampling in feature selection.
      # You should set the same number of and order with iteration
      # Or just 1 parameter which means that all iteration will use the same set of parameter

      # AKL supports more than these parameters
      # Detailed information could be found at
      # https://lightgbm.readthedocs.io/en/latest/Parameters.html
      n_estimators: 1500,2000,2500 #2000
      max_depth: 5,6,8 #8
      num_leaves: 50,100,150 #50
      min_data_in_leaf: 100
      learning_rate: 0.1,0.05,0.03 #0.03
      random_state: 123
      reg_alpha: 0.1
      reg_lambda: 0.1
      sub_feature: 0.8
      bagging_freq: 1
      bagging_fraction: 0.9
  CV: 4               # Number of cross validation folds, cannot be set to 1
  # feature importance is relative to the feature with maximum importance
  min_imp: 0.0001     # minimum importance required for selection #.00001
  inc_imp: 0.0001     # size of increment in cut-off threshold #.00001
  min_var: 100        # stopping condition 1: stop after cutting below this threshold
  max_step: 25        # stopping condition 2: stop after reaching number of iterations
  min_cut: 0.05
  max_cut: 0.9
  # After V0.5, we introduce min_cut and max_cut paremeters which are option.
  # These parameters are controling the presentage of features being dropped at each iteration.
  # min_cut's default value is 0 and max_cut's default value is 1
  # min_cut should be smaller than max_cut, and both are ranging from 0 to 1
  # min_cut represents the min presentage of feature being dropped at each iteration.
  # max_cut represents the max presentage of feature being dropped at each iteration.
  output_iteration: -1
  # After V0.5, we support an new function that user can select the iteration they want
  # If you put best, we will select the best iteration, according to the validation score
  # If you put integer like 5, we will select the features from the 5th iteration
  # If you put -1, we will just output the features selected from the last iteration
  forceInputFeature:
  - f0_spend_hurdle
  - f0_reward
#    - f0_avg_basket
#    - f0_avg_wine
#    - f0_avg_spirits
#    - f0_avg_beer
  - f0_campaign_type
  - f0_campaign_level
  - f0_campaign_category  
  - f0_offer_duration_days
#    - f0_multiplier
#    - f0_campaign_level
#    - f0_cvm
#    - f0_campaign_duration
#    - f0_camp_rdm_flag
  # - ''
      # White list of features that be forced into fitting
      # Example:
      # - 'f0_campaign_type'
# ------------------------------------------------------------------------------
fit:
  training_params:
    label_encode: true
    early_stopping_rounds: 50
    nfolds: 5                 # Number of cross validation folds, can be set to 1
    train_rate: 70            # If nfolds=1, 70% of data will be used for training
    random_state: 777
  algo_max_tune:
    # Number of random grid search iterations for each boosting type
    gbdt: 10
    rf: 1
    # dart: 4 (optional)
    # AKL supports more than these parameters
    # Detailed information could be found at
    # https://lightgbm.readthedocs.io/en/latest/Parameters.html
  gbdt_hyper_params:
    n_estimators:  1000, 1500, 2000, 2500, 5000
    num_leaves: 31, 50, 60, 70, 100
    learning_rate: 0.01, 0.015 ,0.02
    max_depth: 4, 6, 8, 10
    min_child_samples: 100,200,500
    reg_alpha: 0.01, 0.02, 0.03, 0.5, 1
    reg_lambda: 0.01, 0.02, 0.04, 0.5 , 1
    random_state: 777
  rf_hyper_params:
    bagging_freq: 1,5,10
    bagging_fraction: 0.8, 0.7
    n_estimators:  1000, 1500, 2000
    num_leaves: 31, 50, 60, 70
    learning_rate: 0.01, 0.015 ,0.02
    max_depth: 4, 6, 8
    min_child_samples: 100,200,500
    reg_alpha: 0.01, 0.02, 0.03
    reg_lambda: 0.01, 0.02, 0.04
    random_state: 777
model_selector:
  user_specified_model: ''
      # Default to '', AKL selects iteration with best validation performance
      # User can overwrite with iteration number
      # Use diagnostic dashboard to see performance between iterations
      # Option 1: select the best validation performance
      # user_specified_model: ''
      # Option 2: select user define model like the first one
      # user_specified_model: 1
diagnosis:
  diag_groups: 10
      # Optional parameter and the default one is 10
      # Set number of groups for each feature to calculate the mean of prediction and actual
      # If your category feature unique value is larger than this, 
      # We will set to the number of max category feature unique value
  optional_features:
  - f0_campaign_category
      # Additional features for goodness of fit plots
  shap_para:
    cutoff_method: quantile # quantile or absolute
    min: 0.01
    max: 0.99
  metrics:
  - auc
  - binary_logloss
  # After V0.4c, we support an new function that user can select metrics to be calculated in diag
  # For regression we have:
  #  explained_variance_score,mean_absolute_error,mean_squared_error,
  #  mean_absolute_error,mean_squared_log_error,median_absolute_error
  # For classification we have:
  #  accuracy_score,auc,average_precision_score,
  #  f1_score,log_loss,binary_logloss,
  #  precision_score,r2_score, r2,recall_score,zero_one_loss,binary_error